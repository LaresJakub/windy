//
//  DailyWeather.swift
//  Windy
//
//  Created by Jakub Lares on 01.11.15.
//  Copyright © 2015 Jakub Lares. All rights reserved.
//

import Foundation
import UIKit

struct DailyWeather {
    
    let maxTemperature: Int?
    let minTemperature: Int?
    let humidity: Int?
    let precipChance: Int?
    let dateFormatter = NSDateFormatter()
    
    var icon: UIImage? = UIImage(named: "default.png")
    var largeIcon: UIImage? = UIImage(named: "default_large.png")
    var summary: String?
    var sunriseTime: String?
    var sunsetTime: String?
    var day: String?
    
    init(dailyWeatherDict: [String: AnyObject]) {
        
        maxTemperature = dailyWeatherDict["temperatureMax"] as? Int
        minTemperature = dailyWeatherDict["temperatureMin"] as? Int
        summary = dailyWeatherDict["summary"] as? String
        
        if let humidityFloat = dailyWeatherDict["humidity"] as? Double {
            humidity = Int(humidityFloat * 100)
        } else {
            humidity = nil
        }
        
        if let precipChanceFloat = dailyWeatherDict["precipProbability"] as? Double {
            precipChance = Int(precipChanceFloat * 100)
        } else {
            precipChance = nil
        }
        
        
        if let iconString = dailyWeatherDict["icon"] as? String,
        let iconEnum = Icon(rawValue: iconString){
            (icon, largeIcon) = iconEnum.toImage()
        }
        
        if let sunriseDate = dailyWeatherDict["sunriseTime"] as? Double {
            sunriseTime = timeStringFromUnicTime(sunriseDate)
        } else {
            sunriseTime = nil
        }
        
        if let sunsetDate = dailyWeatherDict["sunsetTime"] as? Double {
            sunsetTime = timeStringFromUnicTime(sunsetDate)
        } else {
            sunsetTime = nil
        }
        
        if let time = dailyWeatherDict["time"] as? Double {
            day = dayStringFromTime(time)
        } else {
            day = nil
        }
    }
    
    func timeStringFromUnicTime(unixTime: Double) -> String {
        let date = NSDate(timeIntervalSince1970: unixTime)
        
        // returns date formatted as 24 hour time.
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.stringFromDate(date)
    }
    
    func dayStringFromTime(time: Double) -> String {
        let date = NSDate(timeIntervalSince1970: time)
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.stringFromDate(date)
    }
}
