//
//  CurrentWeather.swift
//  Windy
//
//  Created by Jakub Lares on 28.10.15.
//  Copyright © 2015 Jakub Lares. All rights reserved.
//

import Foundation
import UIKit


struct CurrentWeather {
    let temperature: Int?
    let humidity: Int?
    let precipPropability: Int?
    let summary: String?
    var icon: UIImage? = UIImage(named: "default.png")
    
    init (weatherDictionary: [String : AnyObject]) {
        temperature = weatherDictionary["temperature"] as? Int
        if let humidityFloat = weatherDictionary["humidity"] as? Double {
            humidity = Int(humidityFloat * 100)
        } else {
            humidity = nil
        }

        if let precipProbabilityFloat = weatherDictionary["precipProbability"] as? Double {
            precipPropability = Int(precipProbabilityFloat * 100)
        } else {
            precipPropability = nil
        }

        summary = weatherDictionary["summary"] as? String
        
        if let iconString = weatherDictionary["icon"] as? String,
            let weatherIcon: Icon = Icon(rawValue: iconString){
            (icon, _) = weatherIcon.toImage()
        }
    }
}
