//
//  NetworkOperation.swift
//  Windy
//
//  Created by Jakub Lares on 30.10.15.
//  Copyright © 2015 Jakub Lares. All rights reserved.
//

import Foundation

class NetworkOperation {

    lazy var config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
    lazy var session: NSURLSession = NSURLSession(configuration: self.config)
    let queryURL: NSURL
    
    typealias JSONDictionaryCompletion = ([String: AnyObject]? -> Void)
    
    init(url: NSURL) {
        self.queryURL = url
    }
    
    func downloadJSONFromURL(completion: JSONDictionaryCompletion) {
        
        let request: NSURLRequest = NSURLRequest(URL: queryURL)
        let dataTask = session.dataTaskWithRequest(request) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
           
            // 1. Check HTTP response for successful GET request
            if let httpResponse = response as? NSHTTPURLResponse {
                
                switch (httpResponse.statusCode) {
            
                case 200:
                     // 2. Create JSON object with data
                    do {
                        let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject]
                        completion(jsonDictionary)
                    } catch let error {
                        print(error)
                    }
                
                default:
                    print("GET request not successful. HTTP status code \(httpResponse.statusCode)")
                    print(self.queryURL)
                }
                
            } else {
                print("Error: Not a valid HTTP response")
            }
        }
        dataTask.resume()
    }
}