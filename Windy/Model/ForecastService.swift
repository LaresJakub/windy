//
//  ForecastService.swift
//  Windy
//
//  Created by Jakub Lares on 30.10.15.
//  Copyright © 2015 Jakub Lares. All rights reserved.
//

import Foundation

struct ForecastService {
    
    let forecastAPIKey: String
    let forecastBaseURL: NSURL?
    
    init(APIKey: String) {
        forecastAPIKey = APIKey
        forecastBaseURL = NSURL(string: "https://api.forecast.io/forecast/\(forecastAPIKey)/")
    }
    
    func getForecast(lat: Double, long: Double, completion: (Forecast? -> Void)) {
        if let forecastURL = NSURL(string: "\(lat),\(long)", relativeToURL: forecastBaseURL) {
            
            let networkOperation = NetworkOperation(url: forecastURL)
            networkOperation.downloadJSONFromURL({ (JSONDictionary: [String : AnyObject]?) -> Void in
                let forecast = Forecast(weatherDictionary: JSONDictionary)
                completion(forecast)
            })
            
        } else {
            print("Could not construct a valid URL")
        }
    }
}